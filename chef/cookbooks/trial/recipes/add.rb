include_recipe "route53"
require 'socket'

public_ip = `curl http://169.254.169.254/latest/meta-data/public-ipv4`

route53_record "creating a record" do
  name "mamba"
  value public_ip
  type "A"
  zone_id "example.com"
  aws_access_key_id "XXX"
  aws_secret_access_key "YYY"
  overwrite true
  action :create
end
