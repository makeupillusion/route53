name             "trial"
maintainer       "Dan Vaida"
maintainer_email "vaida.dan@gmail.com"
license          "Apache 2.0"
description      "Creates and deletes a DNS record in Route53"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.0"
