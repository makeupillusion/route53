#Tip
Before playing with your DNS, be sure you make a backup first.
You can use your tool of choice or https://github.com/barnybug/cli53

#About
This simple playbook demonstrates how easy it is to create or delete a
DNS record in Route53 with Ansible.

For this simple demonstration I did not choose to use a dynamic inventory
for Ansible, hence I am using the meta-data from AWS to provide me the public IP
address of the instance. 

#Requirements 
* an instance
* boto (for talking to AWS SDK)
* ansible

#Config
* put your instance's IP in the hosts file
* amend *remote_user* and *private_key_file* in the **ansible.cfg**

#Run
The following is ran from a control machine against the instance:
```
ansible-playbook route53.yml --extra-vars "action=create"
```
or 
```
ansible-playbook route53.yml --extra-vars "action=delete"
```
If you want to run the playbook on the same instance that invokes it,
simply add *connection: local* under *hosts: [...]* in **route53.yml**
